DROP VIEW jobs_pp;

-- Create a view extracting some interesting fields and adding the job_group field

CREATE VIEW jobs_pp AS
SELECT SUBSTR(name, 0, iif(instr(name, ' '), instr(name, ' '), iif(instr(name, ':'), instr(name, ':'), length(name)+1))) AS job_group,
       *
FROM
  (SELECT id,
          json_extract(JSON, "$.stage") AS stage,
          json_extract(JSON, "$.status") AS status,
          json_extract(JSON, "$.name") AS name,
          json_extract(JSON, "$.duration") AS duration,
          json_extract(JSON, "$.queued_duration") AS queued_duration,
          json_extract(JSON, "$.ref") AS REF,
          json_extract(JSON, "$.created_at") AS created_at,
          json_extract(JSON, "$.pipeline.id") AS pipeline_id,
          json_extract(JSON, "$.pipeline.source") AS pipeline_source,
          json_extract(JSON, "$.user.username") AS username,
          json_extract(JSON, "$.web_url") AS web_url
   FROM jobs);


CREATE VIEW opam_jobs AS
SELECT *
FROM jobs_pp
WHERE job_group = "opam"
  AND duration > 0
  AND (status = "success"
       OR status = "failed");

-- Show where the largest proportion of the duration is spent

SELECT COUNT,
       job_group,
       cast(total_duration/3600 AS int) || ':' || strftime('%M:%S', total_duration/86400.0) AS total_duration,
       printf("%.2f%%", proportion * 100) AS proportion
FROM
  (SELECT COUNT(*) AS COUNT,
          job_group,
          name,
          SUM(duration) AS total_duration,
          SUM(duration)/
     (SELECT SUM(json_extract(JSON, "$.duration"))
      FROM jobs) AS proportion
   FROM jobs_pp
   GROUP BY job_group
   ORDER BY total_duration DESC)
LIMIT 10 ;

-- Some statistics on the opam jobs
WITH opam_jobs AS
  (SELECT *
   FROM jobs_pp
   WHERE job_group = "opam"
     AND duration > 0
     AND (status = "success"
          OR status = "failed"))
SELECT
  (SELECT duration
   FROM opam_jobs
   ORDER BY duration
   LIMIT 1
   OFFSET
     (SELECT count(*)
      FROM opam_jobs)/2) AS duration_median,

  (SELECT min(duration)
   FROM opam_jobs) AS duration_min,

  (SELECT max(duration)
   FROM opam_jobs) AS duration_max,

  (SELECT avg(duration)
   FROM opam_jobs) AS duration_avg ;

-- Are the opam jobs running on main or in merge request pipelines?
WITH opam_jobs AS
  (SELECT *
   FROM jobs_pp
   WHERE job_group = "opam"
     AND duration > 0
     AND (status = "success"
          OR status = "failed"))
SELECT REF,
       total_duration,
       total_duration/
  (SELECT sum(duration)
   FROM opam_jobs)
FROM
  (SELECT REF,
          SUM(duration) AS total_duration
   FROM opam_jobs
   GROUP BY (REF = "master"))
ORDER BY total_duration DESC
LIMIT 10;

-- Are the opam jobs triggered by nomadic-margebot or other users?
-- TODO: double check

SELECT *
FROM
  (SELECT username = "nomadic-margebot" AS is_margebot,
          REF = "master" AS on_master,
                count(*),
                sum(duration)/3600 AS total_time_h,
                sum(duration)/
     (SELECT sum(duration)
      FROM opam_jobs) proportion_time
   FROM opam_jobs
   GROUP BY (username = "nomadic-margebot"), (REF = "master"))
ORDER BY is_margebot,
         on_master DESC;

-- Number of jobs

SELECT count(*)
FROM jobs ;

-- Number of pipelines:

SELECT count(*)
FROM
  (SELECT count(*)
   FROM jobs_pp
   GROUP BY pipeline_id);

-- In the period
WITH jobs_ AS
  (SELECT json_extract(JSON, "$.created_at") AS created_at,
          json_extract(JSON, "$.pipeline.id") pipeline_id
   FROM jobs)
SELECT min(created_at),
       max(created_at)
FROM jobs_;

-- Total sequential time in the period

SELECT sum(duration) / 3600
FROM jobs_pp;

-- Number of jobs and total duration of a given pipeline:

SELECT COUNT,
       cast(total_duration/3600 AS int) || ':' || strftime('%M:%S', total_duration/86400.0) AS total_duration
FROM
  (SELECT count(*) AS COUNT,
          sum(duration) AS total_duration
   FROM jobs_pp
   WHERE pipeline_id = 833616575);


SELECT sum(duration) / 3600
FROM jobs_pp
WHERE job_group = "opam";


SELECT sum(duration) / 3600
FROM jobs_pp
WHERE job_group = "opam"
  AND ((username = "nomadic-margebot")) ;

AS TIME,
   sum(duration)/
  (SELECT sum(duration)
   FROM opam_jobs) proportion_time
FROM opam_jobs
GROUP BY (username = "nomadic-margebot"), (REF = "master"))
ORDER BY proportion_time DESC;

-- Number of jobs

SELECT count(*)
FROM jobs ;

-- Number of pipelines:

SELECT count(*)
FROM
  (SELECT count(*)
   FROM jobs_pp
   GROUP BY pipeline_id);

-- In the period

SELECT min(created_at),
       max(created_at)
FROM jobs_pp;

-- Total sequential time in the period

SELECT sum(duration) / 3600,
       sum(queued_duration) / 3600,
       count(id)
FROM jobs_pp;

-- Total sequential time for a given period

SELECT sum(duration),
       sum(queued_duration),
       count(id)
FROM jobs_pp
WHERE created_at >= "2023-04-01T00:00:00.000Z"
  AND created_at < "2023-05-01T00:00:00.000Z";

-- Number of jobs and total duration of a given pipeline:

SELECT COUNT,
       cast(total_duration/3600 AS int) || ':' || strftime('%M:%S', total_duration/86400.0) AS total_duration
FROM
  (SELECT count(*) AS COUNT,
          sum(duration) AS total_duration
   FROM jobs_pp
   WHERE pipeline_id = 833616575);

-- Leaf opam packages

DROP VIEW leaf_opam_packages;


CREATE VIEW leaf_opam_packages AS
VALUES ("opam:octez-accuser-PtLimaPt")
UNION
VALUES ("opam:octez-accuser-PtMumbai") UNION VALUES ("opam:octez-accuser-PtNairob") UNION VALUES ("opam:octez-baker-PtLimaPt") UNION VALUES ("opam:octez-baker-PtMumbai") UNION VALUES ("opam:octez-baker-PtNairob") UNION VALUES ("opam:octez-client") UNION VALUES ("opam:octez-codec") UNION VALUES ("opam:octez-node") UNION VALUES ("opam:octez-protocol-compiler") UNION VALUES ("opam:octez-proxy-server") UNION VALUES ("opam:octez-signer") UNION VALUES ("opam:octez-smart-rollup-client-PtLimaPt") UNION VALUES ("opam:octez-smart-rollup-client-PtMumbai") UNION VALUES ("opam:octez-smart-rollup-client-PtNairob") UNION VALUES ("opam:octez-smart-rollup-node-PtLimaPt") UNION VALUES ("opam:octez-smart-rollup-node-PtMumbai") UNION VALUES ("opam:octez-smart-rollup-node-PtNairob") ;


SELECT count(*),
       sum(duration) / 3600 AS total_time_h
FROM opam_jobs;


DROP VIEW opam_jobs_necessary;


CREATE VIEW opam_jobs_necessary AS
SELECT *
FROM opam_jobs
WHERE ((username = "nomadic-margebot")
       OR (REF = "master")
       OR name in
         (SELECT *
          FROM leaf_opam_packages)) ;


SELECT count(*),
       sum(duration) / 3600 AS total_time_h
FROM opam_jobs_necessary;


SELECT *
FROM
  (SELECT username = "nomadic-margebot" AS is_margebot,
          REF = "master" AS on_master,
                count(*),
                sum(duration)/3600 AS total_time_h,
                sum(duration)/
     (SELECT sum(duration)
      FROM opam_jobs) proportion_time
   FROM opam_jobs_necessary
   GROUP BY (username = "nomadic-margebot"), (REF = "master"))
ORDER BY is_margebot,
         on_master DESC;

-- all necessary jobs

SELECT count(*),
       sum(duration) / 3600 AS total_time_h
FROM jobs_pp
WHERE (job_group <> "opam"
       OR (username = "nomadic-margebot")
       OR (REF = "master")
       OR name in
         (SELECT *
          FROM leaf_opam_packages)) ;

-- Counting the number of hangs per day for a given job (here, [unit:js_components]):

SELECT name,
       created_at_date,
       count(*) runs,
       sum(duration >= 3600) hangs,
       (sum(duration >= 3600)*1.0 / (count(*)*1.0) * 100.0) hangs_pct
FROM
  (SELECT name,
          created_at,
          status,
          duration,
          SUBSTR(created_at, 0, INSTR(created_at, 'T')) AS created_at_date
   FROM jobs_pp
   WHERE name = "unit:js_components")
GROUP BY created_at_date
ORDER BY created_at_date DESC;

-- Counting frequency of pipelines per 'pipeline type' (used for
-- https://gitlab.com/tezos/pipeline-profiler/-/issues/1).

SELECT count(DISTINCT pipeline_id) AS COUNT,
       pipeline_source,
       is_master,
       is_margebot,
       has_opam,
       printf("%.2fh", sum(duration)/3600) AS total_time_h,
       printf("%.2f%%", (sum(duration)/
                           (SELECT sum(duration)
                            FROM jobs_pp)) * 100) AS total_time_prop
FROM
  (SELECT pipeline_id,
          pipeline_source,
          sum(duration) AS duration,
          REF = "master" AS is_master,
                username = "nomadic-margebot" AS is_margebot,
                sum(job_group = "opam") > 0 AS has_opam
   FROM jobs_pp
   WHERE (status = "success"
          OR status = "failed")
   GROUP BY pipeline_id)
GROUP BY pipeline_source,
         is_master,
         is_margebot,
         has_opam;