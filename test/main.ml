(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Glsqlml
module Log = Tezt.Log

let wrap f =
  let* res = f in
  match res with
  | Result.Ok () -> unit
  | Result.Error err -> Test.fail "Caqti_error: %s" (Caqti_error.show err)

let () =
  Test.register ~title:"DB roundtrip" ~tags:[ "db"; "job" ] ~__FILE__
  @@ fun () ->
  wrap
  @@
  let open Lwt_result.Syntax in
  let db = Temp.file "test.db" in
  let* (module Db : Caqti_lwt.CONNECTION) =
    Caqti_lwt.connect (Uri.of_string @@ "sqlite3://" ^ db)
  in
  Log.info "Create database";
  let* () = Db.exec Job.Q.create () in
  Log.info "Insert one job";
  let dummy = Job.dummy in
  let* count = Db.exec_with_affected_count Job.Q.insert dummy in
  Check.(
    (1 = count) ~__LOC__ int ~error_msg:"Expected exactly one inserted row");
  let* latest_opt = Db.find_opt Job.Q.latest () in
  (match latest_opt with
  | Some latest ->
      Check.(
        (dummy = latest) ~__LOC__ Job.tezt_typ ~error_msg:"Expected %L, got %R")
  | None -> Test.fail "expected to get the one row back");
  Log.info "Upsert";
  let dummy = { dummy with status = "failed" } in
  let* count = Db.exec_with_affected_count Job.Q.upsert dummy in
  Check.(
    (1 = count) ~__LOC__ int ~error_msg:"Expected exactly one inserted row");
  let* latest_opt = Db.find_opt Job.Q.latest () in
  (match latest_opt with
  | Some latest ->
      Check.(
        (dummy = latest) ~__LOC__ Job.tezt_typ ~error_msg:"Expected %L, got %R")
  | None -> Test.fail "expected to get the one row back");
  Lwt_result.return ()

let () =
  Test.register ~title:"of_json" ~tags:[ "of_json"; "job" ] ~__FILE__
  @@ fun () ->
  let json = JSON.parse_file Base.(project_root // "test/job.json") in
  let job = Job.of_json ~sections:Job_sections.empty ~trace:None json in
  let expected_job : Job.t =
    {
      id = 5561948632;
      json;
      stage = "trigger";
      status = "success";
      name = "trigger";
      duration = Some 18.641593;
      queued_duration = Some 2.690172;
      ref_ = "refs/merge-requests/10879/head";
      created_at = "2023-11-17T14:00:12.991Z";
      pipeline_id = 1076380776;
      pipeline_source = "merge_request_event";
      username = "nomadic-margebot";
      web_url = "https://gitlab.com/tezos/tezos/-/jobs/5561948632";
      sections = Job_sections.empty;
      tag_list = [ "gcp"; "foobar" ];
      failure_reason = None;
      trace = None;
    }
  in
  Check.(
    (expected_job = job) ~__LOC__ Job.tezt_typ
      ~error_msg:"Expected job %L, got %R");
  unit

let db_version_typ : Db_version.t Check.typ =
  Check.equalable
    (fun fmt v -> Format.fprintf fmt "%d" (Db_version.to_int v))
    ( = )

let () =
  Test.register ~title:"db_version" ~tags:[ "db"; "db_version" ] ~__FILE__
  @@ fun () ->
  wrap
  @@
  let open Lwt_result.Syntax in
  let db = Temp.file "test.db" in
  let* (module Db : Caqti_lwt.CONNECTION) =
    Caqti_lwt.connect (Uri.of_string @@ "sqlite3://" ^ db)
  in
  Log.info "Create [db_version] table";
  let* exists = Db.find Db_version.Q.exists () in
  Check.(is_false exists)
    ~error_msg:"Did not expect the versioning table to exist";
  let* () = Db.exec Db_version.Q.create () in
  let* exists = Db.find Db_version.Q.exists () in
  Check.(is_true exists) ~error_msg:"Expected the versioning table to exist";
  let* version_opt = Db.find_opt Db_version.Q.get () in
  Check.(
    (version_opt = None) (option db_version_typ)
      ~error_msg:"Did not expect any version");
  let* aff = Db.exec_with_affected_count Db_version.Q.set V0 in
  Check.((aff = 1) int ~error_msg:"Expected %R affected, got %L");
  let* version_opt = Db.find_opt Db_version.Q.get () in
  Check.(
    (version_opt = Some V0) (option db_version_typ)
      ~error_msg:"Expected version V0");
  let* aff = Db.exec_with_affected_count Db_version.Q.set V1 in
  Check.((aff = 1) int ~error_msg:"Expected %R affected, got %L");
  let* version_opt = Db.find_opt Db_version.Q.get () in
  Check.(
    (version_opt = Some V1) (option db_version_typ)
      ~error_msg:"Expected version V1");
  Lwt_result.return ()

let () =
  Test.register ~title:"create" ~tags:[ "db"; "creation" ] ~__FILE__
  @@ fun () ->
  wrap
  @@
  let open Lwt_result.Syntax in
  let db = Temp.file "test.db" in
  let* (module Db : Caqti_lwt.CONNECTION) =
    Caqti_lwt.connect (Uri.of_string @@ "sqlite3://" ^ db)
  in
  Log.info "Create a database";
  let* () = Schema.create (module Db) in
  let* exists = Db.find Db_version.Q.exists () in
  Check.(is_true exists) ~error_msg:"Expected the versioning table to exist";
  Log.info "Check db version";
  let* version_opt = Db.find_opt Db_version.Q.get () in
  Check.(
    (version_opt = Some Db_version.latest)
      (option db_version_typ)
      ~error_msg:"Expected version %R after migration, got %L");
  Lwt_result.return ()

let () = Test.run ()
