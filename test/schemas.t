Check schemas and versions:

  $ sqlite3 test_v0.db '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT);

  $ sqlite3 test_v1.db '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT, failure_reason TEXT);
  CREATE TABLE db_version (
      id INTEGER PRIMARY KEY CHECK (id = 0),
      version INTEGER
  );
  $ sqlite3 test_v1.db -line 'select * from db_version'
       id = 0
  version = 1

Check creation:

  $ ../bin/main.exe --db test.db --create-only 2>&1 | sed 's/\[[[:digit:]:\. \-]\+\] //'
  INFO: Creating database test.db
  $ sqlite3 test.db '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT, failure_reason TEXT, trace TEXT);
  CREATE TABLE db_version (
      id INTEGER PRIMARY KEY CHECK (id = 0),
      version INTEGER
  );
  $ sqlite3 test.db -line 'select * from db_version'
       id = 0
  version = 2

Check migration from v0:
  $ db_v0=$(mktemp); cp test_v0.db $db_v0
  $ sqlite3 $db_v0 -line 'begin transaction; update jobs set json = "{}" where id = (select id from jobs where status = "failed" limit 1); select * from jobs where status = "failed" limit 1; rollback'
                      id = 5680550708
                    json = {}
         resolve_secrets = 
        prepare_executor = 
          prepare_script = 
             get_sources = 
           restore_cache = 
             step_script = 
           archive_cache = 
  cleanup_file_variables = 
                   stage = test_coverage
                  status = failed
                    name = unified_coverage
                duration = 174.810712
         queued_duration = 1.351979
                     ref = refs/merge-requests/11155/head
              created_at = 2023-12-05T06:35:35.440Z
             pipeline_id = 1095461510
         pipeline_source = merge_request_event
                username = philippewang.info
                 web_url = https://gitlab.com/tezos/tezos/-/jobs/5680550708
                tag_list = gcp
  $ sqlite3 $db_v0 -line 'select count(*) from jobs limit 1'
  count(*) = 1200
  $ ../bin/main.exe --db $db_v0 --create-only 2>&1 | sed 's/\[[[:digit:]:\. \-]\+\] //' | sed 's@/tmp[^ ]\+@/tmp/...@'
  INFO: Migrated database /tmp/... from V0 to V2
  $ sqlite3 $db_v0 '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT, failure_reason TEXT, trace TEXT);
  CREATE TABLE db_version (
      id INTEGER PRIMARY KEY CHECK (id = 0),
      version INTEGER
  );
  $ sqlite3 $db_v0 -line 'select * from db_version'
       id = 0
  version = 2
  $ sqlite3 $db_v0 -line 'begin transaction; update jobs set json = "{}" where id = (select id from jobs where status = "failed" limit 1); select * from jobs where status = "failed" limit 1; rollback'
                      id = 5680550708
                    json = {}
         resolve_secrets = 
        prepare_executor = 
          prepare_script = 
             get_sources = 
           restore_cache = 
             step_script = 
           archive_cache = 
  cleanup_file_variables = 
                   stage = test_coverage
                  status = failed
                    name = unified_coverage
                duration = 174.810712
         queued_duration = 1.351979
                     ref = refs/merge-requests/11155/head
              created_at = 2023-12-05T06:35:35.440Z
             pipeline_id = 1095461510
         pipeline_source = merge_request_event
                username = philippewang.info
                 web_url = https://gitlab.com/tezos/tezos/-/jobs/5680550708
                tag_list = gcp
          failure_reason = script_failure
                   trace = 
  $ sqlite3 $db_v0 -line 'select count(*) from jobs limit 1'
  count(*) = 1200

Check migration from v1:

  $ db_v1=$(mktemp); cp test_v1.db $db_v1
  $ sqlite3 $db_v1 -line 'select * from db_version'
       id = 0
  version = 1
  $ sqlite3 $db_v1 -line 'begin transaction; update jobs set json = "{}" limit 1; select * from jobs limit 1; rollback' | tee job
                      id = 5680458893
                    json = {}
         resolve_secrets = 
        prepare_executor = 
          prepare_script = 
             get_sources = 
           restore_cache = 
             step_script = 
           archive_cache = 
  cleanup_file_variables = 
                   stage = build
                  status = canceled
                    name = build_arm64-exp-dev-extra
                duration = 
         queued_duration = 
                     ref = refs/merge-requests/11125/head
              created_at = 2023-12-05T06:07:37.596Z
             pipeline_id = 1095445051
         pipeline_source = merge_request_event
                username = sras
                 web_url = https://gitlab.com/tezos/tezos/-/jobs/5680458893
                tag_list = gcp_arm64
          failure_reason = 
  $ sqlite3 $db_v1 -line 'select count(*) from jobs limit 1'
  count(*) = 1200
  $ ../bin/main.exe --db $db_v1 --create-only 2>&1 | sed 's/\[[[:digit:]:\. \-]\+\] //' | sed 's@/tmp[^ ]\+@/tmp/...@'
  INFO: Migrated database /tmp/... from V1 to V2
  $ sqlite3 $db_v1 '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT, failure_reason TEXT, trace TEXT);
  CREATE TABLE db_version (
      id INTEGER PRIMARY KEY CHECK (id = 0),
      version INTEGER
  );
  $ sqlite3 $db_v1 -line 'select * from db_version'
       id = 0
  version = 2
  $ sqlite3 $db_v1 -line 'begin transaction; update jobs set json = "{}" limit 1; select * from jobs limit 1; rollback' > job2
  $ diff job job2 || true
  23a24
  >                  trace = 
  $ sqlite3 $db_v1 -line 'select count(*) from jobs limit 1'
  count(*) = 1200

Check migration from v2:

  $ db_v2=$(mktemp); cp test_v2.db $db_v2
  $ sqlite3 $db_v2 -line 'select * from db_version'
       id = 0
  version = 2
  $ sqlite3 $db_v1 -line 'begin transaction; update jobs set json = "{}" limit 1; select * from jobs limit 1; rollback' | tee job
                      id = 5680458893
                    json = {}
         resolve_secrets = 
        prepare_executor = 
          prepare_script = 
             get_sources = 
           restore_cache = 
             step_script = 
           archive_cache = 
  cleanup_file_variables = 
                   stage = build
                  status = canceled
                    name = build_arm64-exp-dev-extra
                duration = 
         queued_duration = 
                     ref = refs/merge-requests/11125/head
              created_at = 2023-12-05T06:07:37.596Z
             pipeline_id = 1095445051
         pipeline_source = merge_request_event
                username = sras
                 web_url = https://gitlab.com/tezos/tezos/-/jobs/5680458893
                tag_list = gcp_arm64
          failure_reason = 
                   trace = 
  $ sqlite3 $db_v2 -line 'select count(*) from jobs limit 1'
  count(*) = 1200
  $ ../bin/main.exe --db $db_v2 --create-only 2>&1 | sed 's/\[[[:digit:]:\. \-]\+\] //' | sed 's@/tmp[^ ]\+@/tmp/...@'
  $ sqlite3 $db_v2 '.schema'
  CREATE TABLE jobs (id INTEGER PRIMARY KEY NOT NULL, json TEXT NOT NULL, resolve_secrets INTEGER, prepare_executor INTEGER, prepare_script INTEGER, get_sources INTEGER, restore_cache INTEGER, step_script INTEGER, archive_cache INTEGER, cleanup_file_variables INTEGER, stage TEXT, status TEXT, name TEXT, duration REAL, queued_duration REAL, ref TEXT, created_at TEXT, pipeline_id INTEGER, pipeline_source TEXT, username TEXT, web_url TEXT, tag_list TEXT, failure_reason TEXT, trace TEXT);
  CREATE TABLE db_version (
      id INTEGER PRIMARY KEY CHECK (id = 0),
      version INTEGER
  );
  $ sqlite3 $db_v2 -line 'select * from db_version'
       id = 0
  version = 2
  $ sqlite3 $db_v2 -line 'begin transaction; update jobs set json = "{}" limit 1; select * from jobs limit 1; rollback' > job2
  $ diff job job2 || true
  $ sqlite3 $db_v2 -line 'select count(*) from jobs limit 1'
  count(*) = 1200
