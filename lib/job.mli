(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Represents one job *)
type t = {
  id : int;
  json : JSON.t;  (** JSON *)
  sections : Job_sections.t;  (** Job sections *)
  stage : string;  (** Job details *)
  status : string;
  name : string;
  duration : float option;
  queued_duration : float option;
  ref_ : string;
  created_at : string;
  pipeline_id : int;
  pipeline_source : string;
  username : string;
  web_url : string;
  tag_list : string list;
  failure_reason : string option;
  trace : string option;
}

(** A dummy job for testing. *)
val dummy : t

(** Pretty print a job *)
val pp : Format.formatter -> t -> unit

(** Obtain a job from a json representation.

      [sections] is inserted to the job. *)
val of_json : sections:Job_sections.t -> trace:string option -> JSON.t -> t

(** A {!Tezt.Check.typ} of [t] for testing. *)
val tezt_typ : t Tezt.Check.typ

module Q : sig
  (** Retrieve the id and created_at of the most recent job *)
  val latest : (unit, t, [ `One | `Zero ]) Caqti_request.t

  (** Create the jobs table *)
  val create : (unit, unit, [ `Zero ]) Caqti_request.t

  (** Migrate the [jobs] table from version [migrate_from] to its successor. *)
  val migrate :
    Caqti_lwt.connection ->
    migrate_from:Db_version.t ->
    (unit, Caqti_error.t) Lwt_result.t

  (** Insert a job into the jobs table *)
  val insert : (t, unit, [ `Zero ]) Caqti_request.t

  (** Upsert a job into the jobs table *)
  val upsert : (t, unit, [ `Zero ]) Caqti_request.t
end
