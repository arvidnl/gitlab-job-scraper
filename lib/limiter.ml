(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

open Lwt.Syntax
open Cohttp_lwt_unix
open Util

type rates = { observed : int; remaining : int; reset : int; limit : int }
[@@warning "-69"]

type rate_table =
  | Known of rates
  | Waiting of { limits : unit Lwt.t }
  | Unknown

type state = {
  mutable rate_table : rate_table;
  mutable inflight : int;
  mutable sleeping : int;
  mutable retries : int;
}

let retries (t : state) = t.retries

let state_to_string { rate_table; inflight; sleeping; retries } =
  let rates_to_string = function
    | { observed : int; remaining : int; reset : int; limit : int } ->
        sf
          "{ observed : %d; remaining : %d; reset : %d; limit : %d; retries : \
           %d }"
          observed remaining reset limit retries
  in
  let rate_table_to_string = function
    | Unknown -> "Unknown"
    | Known rates -> sf "Known %s" (rates_to_string rates)
    | Waiting _ -> "Waiting"
  in
  sf "{ inflight = %d; sleeping: %d; rate_table = %s }" inflight sleeping
    (rate_table_to_string rate_table)

let empty () = { rate_table = Unknown; inflight = 0; sleeping = 0; retries = 0 }

let rates_of_response (resp : Response.t) =
  let get header =
    Cohttp.Header.get resp.headers header |> Option.map int_of_string
  in
  (* Printf.printf "Headers: %s\n%!" (Cohttp.Header.to_string resp.headers); *)
  let observed = get "ratelimit-observed" in
  let remaining = get "ratelimit-remaining" in
  let reset = get "ratelimit-reset" in
  let limit = get "ratelimit-limit" in
  match (observed, remaining, reset, limit) with
  | Some observed, Some remaining, Some reset, Some limit ->
      Some { observed; remaining; reset; limit }
  | _ -> None

let rec get (t : state) (req : unit -> (Response.t * Cohttp_lwt.Body.t) Lwt.t) =
  match t.rate_table with
  | Unknown -> (
      (*         Printf.printf "Rates are unknown\n%!"; *)
      let limits, limits_resolver = Lwt.wait () in
      t.rate_table <- Waiting { limits };
      t.inflight <- t.inflight + 1;
      (*         Printf.printf "Requesting rates\n%!"; *)
      let* resp, body = req () in
      t.inflight <- t.inflight - 1;
      match Cohttp.Response.status resp with
      | `Too_many_requests ->
          let delay =
            Cohttp.Header.get resp.headers "Retry-After"
            |> Option.fold ~none:60.0 ~some:float_of_string
          in
          Log.debug "Rate limited, sleeping for %f seconds" delay;
          t.retries <- t.retries + 1;
          t.sleeping <- t.sleeping + 1;
          let* () = Lwt_unix.sleep delay in
          t.sleeping <- t.sleeping - 1;
          get t req
      | _ ->
          (*         Printf.printf "Rates request returned\n%!"; *)
          (match rates_of_response resp with
          | Some rates ->
              t.rate_table <- Known rates;
              Log.debug "Got rates: %s" (state_to_string t)
          | _ ->
              (*             Printf.printf "Rates were unknown\n%!"; *)
              t.rate_table <- Unknown);
          Lwt.wakeup_later limits_resolver ();
          Lwt.return (resp, body))
  (*           (fun e -> *)
  (*             Printf.printf "Error when fetching rates\n%!"; *)
  (*             Lwt.wakeup_later_exn limits_resolver e; *)
  (*             t.rate_table <- Unknown; *)
  (*             Lwt.return_unit) *)
  | Waiting { limits } ->
      (*         Printf.printf "Waiting for rates\n%!"; *)
      let* () = limits in
      get t req
  | Known rates ->
      (*         Printf.printf "Rates are known\n%!"; *)
      let remaining = rates.remaining - t.inflight in
      if remaining = 0 then (
        let delay =
          Float.max 0.0 (float_of_int rates.reset -. Unix.gettimeofday ())
        in
        Log.warn "Quota exhausted, sleeping for %f seconds" delay;
        t.sleeping <- t.sleeping + 1;
        let* () = Lwt_unix.sleep delay in
        t.sleeping <- t.sleeping - 1;
        get t req)
      else (
        t.inflight <- t.inflight + 1;
        let* resp, body = req () in
        t.inflight <- t.inflight - 1;
        match Cohttp.Response.status resp with
        | `Too_many_requests ->
            let delay =
              Cohttp.Header.get resp.headers "Retry-After"
              |> Option.fold ~none:60.0 ~some:float_of_string
            in
            Log.debug "Rate limited, waiting for %f seconds" delay;
            t.retries <- t.retries + 1;
            t.sleeping <- t.sleeping + 1;
            let* () = Lwt_unix.sleep delay in
            t.sleeping <- t.sleeping - 1;
            get t req
        | _ ->
            (match rates_of_response resp with
            | Some rates ->
                t.rate_table <- Known rates;
                Log.debug "Updated rates: %s" (state_to_string t)
            | _ -> ());
            Lwt.return (resp, body))
