(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

let sf = Printf.sprintf

let _range a b =
  let rec range ?(acc = []) a b =
    if b < a then acc else range ~acc:(b :: acc) a (b - 1)
  in
  range a b
