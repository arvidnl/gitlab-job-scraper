(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

exception Invalid_version of int

type t = V0 | V1 | V2

let latest = V2

let of_int = function
  | 0 -> V0
  | 1 -> V1
  | 2 -> V2
  | n -> raise (Invalid_version n)

let to_int = function V0 -> 0 | V1 -> 1 | V2 -> 2
let succ = function V0 -> Some V1 | V1 -> Some V2 | V2 -> None

let caqti_typ =
  let open Caqti_type.Std in
  custom
    ~encode:(fun version -> version |> to_int |> Result.ok)
    ~decode:(fun version_int -> version_int |> of_int |> Result.ok)
    int

module Q = struct
  open Caqti_request.Infix
  open Caqti_type.Std

  let exists =
    (unit ->! bool)
      {|SELECT count(*) = 1 FROM sqlite_master WHERE type='table' AND name='db_version';|}

  let create =
    (unit ->. unit)
      {|CREATE TABLE db_version (
    id INTEGER PRIMARY KEY CHECK (id = 0),
    version INTEGER
);|}

  let get = (unit ->? caqti_typ) "SELECT version FROM db_version"

  let set =
    (caqti_typ ->. unit)
      "INSERT OR REPLACE INTO db_version (id, version) VALUES (0, ?)"

  let migrate (module Db : Caqti_lwt.CONNECTION) ~migrate_from =
    match migrate_from with
    | V0 -> Db.exec create ()
    | V1 | V2 -> Lwt_result.return ()
end
