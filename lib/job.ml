(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

open Util

type t = {
  id : int;
  (* JSON *)
  json : JSON.t;
  (* Job sections *)
  sections : Job_sections.t;
  (* Job details *)
  stage : string;
  status : string;
  name : string;
  duration : float option;
  queued_duration : float option;
  ref_ : string;
  created_at : string;
  pipeline_id : int;
  pipeline_source : string;
  username : string;
  web_url : string;
  tag_list : string list;
  failure_reason : string option;
  trace : string option;
}

let dummy =
  {
    id = 0;
    json = JSON.parse ~origin:"dummy" "{}";
    sections = Job_sections.empty;
    stage = "build";
    status = "failed";
    name = "job_name";
    duration = Some 123.456;
    queued_duration = Some 789.999;
    ref_ = "my-branch-name";
    created_at = "2023-11-17T14:00:12.991Z";
    pipeline_id = 123;
    pipeline_source = "push";
    username = "user";
    web_url = "https://example.com/";
    tag_list = [ "foo"; "bar" ];
    failure_reason = Some "job_execution_timeout";
    trace = Some "foo\nbar";
  }

let of_json ~sections ~trace json : t =
  JSON.
    {
      id = json |-> "id" |> as_int;
      json;
      sections;
      (* Details *)
      stage = json |-> "stage" |> as_string;
      status = json |-> "status" |> as_string;
      name = json |-> "name" |> as_string;
      duration = json |-> "duration" |> as_float_opt;
      queued_duration = json |-> "queued_duration" |> as_float_opt;
      ref_ = json |-> "ref" |> as_string;
      created_at = json |-> "created_at" |> as_string;
      pipeline_id = json |-> "pipeline" |-> "id" |> as_int;
      pipeline_source = json |-> "pipeline" |-> "source" |> as_string;
      username = json |-> "user" |-> "username" |> as_string;
      web_url = json |-> "web_url" |> as_string;
      tag_list = json |-> "tag_list" |> as_list |> List.map as_string;
      failure_reason = json |-> "failure_reason" |> as_string_opt;
      trace;
    }

let pp : Format.formatter -> t -> unit =
 fun fmt
     {
       id;
       json;
       sections;
       stage;
       status;
       name;
       duration;
       queued_duration;
       ref_;
       created_at;
       pipeline_id;
       pipeline_source;
       username;
       web_url;
       tag_list;
       failure_reason;
       trace;
     } ->
  let opt_float =
    Option.fold ~none:"None" ~some:(fun f -> "Some " ^ string_of_float f)
  in
  let opt_string = Option.fold ~none:"None" ~some:(fun s -> sf "Some %S" s) in
  Format.fprintf fmt
    "{ id = %d; json = %S; %a; stage = %S; status = %S; name = %S; duration = \
     %s; queued_duration = %s; ref_ = %S; created_at = %S; pipeline_id = %d; \
     pipeline_source = %S; username = %S; web_url = %S; tag_list = [%s]; \
     failure_reason = %s; trace = %s}"
    id (JSON.encode json) Job_sections.pp sections stage status name
    (opt_float duration)
    (opt_float queued_duration)
    ref_ created_at pipeline_id pipeline_source username web_url
    (String.concat ";" @@ List.map (sf "%S") tag_list)
    (opt_string failure_reason)
    (opt_string trace)

let tezt_typ : t Tezt.Check.typ =
  (* Equality modulo JSON origin *)
  let job_eq : t -> t -> bool =
   fun a b ->
    let reset json = JSON.(annotate ~origin:"reset" @@ unannotate json) in
    { a with json = reset a.json } = { b with json = reset b.json }
  in
  Tezt.Check.equalable pp job_eq

let caqti_type =
  let open Caqti_type.Std in
  let encode
      {
        id;
        json;
        sections;
        stage;
        status;
        name;
        duration;
        queued_duration;
        ref_;
        created_at;
        pipeline_id;
        pipeline_source;
        username;
        web_url;
        tag_list;
        failure_reason;
        trace;
      } =
    ( (id, json, sections),
      ( (stage, status, name),
        (duration, queued_duration, ref_, created_at),
        (pipeline_id, pipeline_source, username, web_url),
        (tag_list, failure_reason, trace) ) )
    |> Result.ok
  in
  let decode
      ( (id, json, sections),
        ( (stage, status, name),
          (duration, queued_duration, ref_, created_at),
          (pipeline_id, pipeline_source, username, web_url),
          (tag_list, failure_reason, trace) ) ) =
    {
      id;
      json;
      sections;
      stage;
      status;
      name;
      duration;
      queued_duration;
      ref_;
      created_at;
      pipeline_id;
      pipeline_source;
      username;
      web_url;
      tag_list;
      failure_reason;
      trace;
    }
    |> Result.ok
  in
  let tag_list : string list Caqti_type.t =
    custom
      ~encode:(fun strs -> strs |> String.concat "," |> Result.ok)
      ~decode:(fun str -> str |> String.split_on_char ',' |> Result.ok)
      string
  in
  let json : JSON.t Caqti_type.t =
    custom
      ~encode:(fun json -> json |> JSON.encode |> Result.ok)
      ~decode:(fun str -> str |> JSON.parse ~origin:"sqlite" |> Result.ok)
      string
  in
  custom ~encode ~decode
    (tup2
       (tup3 int json Job_sections.caqti_type)
       (tup4
          (tup3 string string string)
          (tup4 (option float) (option float) string string)
          (tup4 int string string string)
          (tup3 tag_list (option string) (option string))))

module Q = struct
  open Caqti_request.Infix
  open Caqti_type.Std

  let fields : (string * string) list =
    [
      ("id", "INTEGER PRIMARY KEY NOT NULL");
      (* JSON *)
      ("json", "TEXT NOT NULL");
      (* Job sections *)
      ("resolve_secrets", "INTEGER");
      ("prepare_executor", "INTEGER");
      ("prepare_script", "INTEGER");
      ("get_sources", "INTEGER");
      ("restore_cache", "INTEGER");
      ("step_script", "INTEGER");
      ("archive_cache", "INTEGER");
      ("cleanup_file_variables", "INTEGER");
      (* Job details (V0) *)
      ("stage", "TEXT");
      ("status", "TEXT");
      ("name", "TEXT");
      ("duration", "REAL");
      ("queued_duration", "REAL");
      ("ref", "TEXT");
      ("created_at", "TEXT");
      ("pipeline_id", "INTEGER");
      ("pipeline_source", "TEXT");
      ("username", "TEXT");
      ("web_url", "TEXT");
      ("tag_list", "TEXT");
      (* Failure reason (V1) *)
      ("failure_reason", "TEXT");
      (* Job trace (V2) *)
      ("trace", "TEXT");
    ]

  let latest =
    (unit ->? caqti_type) @@ {|SELECT * FROM jobs ORDER BY id DESC LIMIT 1|}

  let create =
    (unit ->. unit)
      (sf "CREATE TABLE jobs (%s)"
         (String.concat ", "
         @@ List.map (fun (name, col_typ) -> name ^ " " ^ col_typ) fields))

  let insert_query =
    sf "INSERT INTO jobs (%s) VALUES (%s)"
      (String.concat ", " @@ List.map fst fields)
      (String.concat ", " @@ List.init (List.length fields) (Fun.const "?"))

  let insert = (caqti_type ->. unit) insert_query

  let upsert_query =
    sf "INSERT OR REPLACE INTO jobs (%s) VALUES (%s)"
      (String.concat ", " @@ List.map fst fields)
      (String.concat ", " @@ List.init (List.length fields) (Fun.const "?"))

  let upsert = (caqti_type ->. unit) @@ upsert_query

  let migrate (module Db : Caqti_lwt.CONNECTION) ~migrate_from =
    let open Lwt_result.Syntax in
    match migrate_from with
    | Db_version.V0 ->
        let alter_query =
          (unit ->. unit) {|ALTER TABLE jobs ADD COLUMN failure_reason TEXT;|}
        in
        let update_query =
          (unit ->. unit)
            {|UPDATE jobs SET failure_reason = json_extract(JSON, "$.failure_reason");|}
        in
        let* () = Db.exec alter_query () in
        let* () = Db.exec update_query () in
        Lwt_result.return ()
    | V1 ->
        let alter_query =
          (unit ->. unit) {|ALTER TABLE jobs ADD COLUMN trace TEXT;|}
        in
        let* () = Db.exec alter_query () in
        Lwt_result.return ()
    | V2 -> Lwt_result.return ()
end
