(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

type level = Debug | Info | Warn | Error

let log_level = ref Info
let set_level level = log_level := level

let date_time () =
  let timestamp = Unix.gettimeofday () in
  let time = Unix.gmtime timestamp in
  Printf.sprintf "%04d-%02d-%02d %02d:%02d:%02d.%03d" (1900 + time.tm_year)
    (1 + time.tm_mon) time.tm_mday time.tm_hour time.tm_min time.tm_sec
    (int_of_float ((timestamp -. float (truncate timestamp)) *. 1000.))

let log ~level fmt =
  Format.kasprintf
    (fun s ->
      match (!log_level, level) with
      | Debug, _
      | Info, (Info | Warn | Error)
      | Warn, (Warn | Error)
      | Error, Error ->
          Printf.eprintf "[%s] %s: %s\n%!" (date_time ())
            (match level with
            | Debug -> "DEBUG"
            | Info -> "INFO"
            | Warn -> "WARN"
            | Error -> "ERROR")
            s
      | _ -> ())
    fmt

let debug fmt = log ~level:Debug fmt
let info fmt = log ~level:Info fmt
let warn fmt = log ~level:Warn fmt
let error fmt = log ~level:Error fmt
