type section_info = { index : int; start_time : int; duration : int option }

type t = {
  resolve_secrets : int option;
  prepare_executor : int option;
  prepare_script : int option;
  get_sources : int option;
  restore_cache : int option;
  step_script : int option;
  archive_cache : int option;
  cleanup_file_variables : int option;
}

val empty : t
val parse_sections : string -> t
val sections_to_string : t -> string
val pp : Format.formatter -> t -> unit

val get_job_sections :
  Limiter.state -> token:string -> project:string -> job:int -> unit -> t Lwt.t

val caqti_type : t Caqti_type.t
