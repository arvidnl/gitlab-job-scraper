(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

type job_scope =
  | Created
  | Pending
  | Running
  | Failed
  | Success
  | Canceled
  | Skipped
  | Waiting_for_resource
  | Manual
[@@warning "-unused-constructor"]

let terminated_job_scopes = [ Failed; Success; Canceled; Skipped ]

let job_scope_to_string = function
  | Created -> "created"
  | Pending -> "pending"
  | Running -> "running"
  | Failed -> "failed"
  | Success -> "success"
  | Canceled -> "canceled"
  | Skipped -> "skipped"
  | Waiting_for_resource -> "waiting_for_resource"
  | Manual -> "manual"

(** See {{: https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs} Jobs API > List project jobs *)
let jobs_uri ?pagination ?per_page ?page ?order_by ?sort ?(scope = []) ~project
    () =
  let uri =
    Uri.of_string
      (Printf.sprintf "https://gitlab.com/api/v4/projects/%s/jobs"
         (Uri.pct_encode project))
  in
  let add_param_opt param_name param_value_opt f uri =
    match param_value_opt with
    | Some param -> Uri.add_query_param' uri (param_name, f param)
    | None -> uri
  in
  let add_array_param param_name param_values f uri =
    List.fold_left
      (fun uri value -> Uri.add_query_param' uri (param_name ^ "[]", f value))
      uri param_values
  in
  uri
  |> add_param_opt "pagination" pagination Fun.id
  |> add_param_opt "per_page" per_page string_of_int
  |> add_param_opt "page" page string_of_int
  |> add_param_opt "order_by" order_by Fun.id
  |> add_param_opt "sort" sort Fun.id
  |> add_array_param "scope" scope job_scope_to_string

let trace_uri ~project ~job () =
  Uri.of_string
    (Printf.sprintf "https://gitlab.com/api/v4/projects/%s/jobs/%d/trace"
       (Uri.pct_encode project) job)
