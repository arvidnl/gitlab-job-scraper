(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

(** Creates a database of the latest version. *)
val create : Caqti_lwt.connection -> (unit, Caqti_error.t) Lwt_result.t

(** Creates a database or migrates it to the latest version.

   It works in the following manner:

   - 1 is the latest version.
   - If the database contains no [db_version] table, then the current version is assumed
     to be [0]. If the [db_version] table contains exactly one row with the value [n],
     then current version is [n]. If the table contains zero or more than one rows, then
     the database is corrupted and no further changes will be made.
   - If the current version is equals the latest version, then nothing is done.
   - If the current version [n], the latest version [m] and [n < m] then
     [Jobs.Vx.migrate] and [Db_version.Vx.migrate] is executed for all [x \in (n+1) .. m].
*)
val migrate :
  Caqti_lwt.connection ->
  ((Db_version.t * Db_version.t) option, Caqti_error.t) Lwt_result.t
