(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

exception Invalid_version of int

(** Database versions *)
type t = V0 | V1 | V2

(** The latest database version *)
val latest : t

(** Version of int *)
val of_int : int -> t

(** Version to int *)
val to_int : t -> int

(** Successor of a version, or [None] if the argument is {!latest} *)
val succ : t -> t option

module Q : sig
  (** Check the existance of the [db_version] table. *)
  val exists : (unit, bool, [ `One ]) Caqti_request.t

  (** Create the [db_version] table *)
  val create : (unit, unit, [ `Zero ]) Caqti_request.t

  (** Get the current database version *)
  val get : (unit, t, [ `One | `Zero ]) Caqti_request.t

  (** Set the current database version *)
  val set : (t, unit, [ `Zero ]) Caqti_request.t

  (** Migrate the [jobs] table from version [migrate_from] to its successor. *)
  val migrate :
    Caqti_lwt.connection -> migrate_from:t -> (unit, Caqti_error.t) Lwt_result.t
end
