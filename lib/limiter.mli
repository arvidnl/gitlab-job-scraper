(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

open Cohttp_lwt_unix

type state

val retries : state -> int
val empty : unit -> state

val get :
  state ->
  (unit -> (Response.t * Cohttp_lwt.Body.t) Lwt.t) ->
  (Response.t * Cohttp_lwt.Body.t) Lwt.t
