(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

type job_scope =
  | Created
  | Pending
  | Running
  | Failed
  | Success
  | Canceled
  | Skipped
  | Waiting_for_resource
  | Manual

val terminated_job_scopes : job_scope list

val jobs_uri :
  ?pagination:string ->
  ?per_page:int ->
  ?page:int ->
  ?order_by:string ->
  ?sort:string ->
  ?scope:job_scope list ->
  project:string ->
  unit ->
  Uri.t

val trace_uri : project:string -> job:int -> unit -> Uri.t
