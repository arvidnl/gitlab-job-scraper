(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <contact@nomadic-labs.com>               *)
(*                                                                           *)
(*****************************************************************************)

exception Database_corrupted of string

open Lwt_result.Syntax

let return = Lwt_result.return

let get_version (module Db : Caqti_lwt.CONNECTION) =
  let* exists = Db.find Db_version.Q.exists () in
  if not exists then return Db_version.V0
  else
    let* version = Db.find_opt Db_version.Q.get () in
    match version with
    | Some v -> return v
    | None -> raise (Database_corrupted "[db_version] table contains no rows")

let create (module Db : Caqti_lwt.CONNECTION) =
  let* () = Db.exec Job.Q.create () in
  let* () = Db.exec Db_version.Q.create () in
  Db.exec Db_version.Q.set Db_version.latest

let migrate (module Db : Caqti_lwt.CONNECTION) =
  let conn = (module Db : Caqti_lwt.CONNECTION) in
  let* old_version = get_version conn in
  if old_version = Db_version.latest then Lwt_result.return None
  else
    let rec loop current_version =
      match Db_version.succ current_version with
      | None -> return ()
      | Some next_version ->
          let* () = Job.Q.migrate conn ~migrate_from:current_version in
          let* () = Db_version.Q.migrate conn ~migrate_from:current_version in
          let* () = Db.exec Db_version.Q.set next_version in
          loop next_version
    in
    let* () = loop old_version in
    return (Some (old_version, Db_version.latest))
