open Util
open Cohttp_lwt_unix

type section_info = { index : int; start_time : int; duration : int option }

type t = {
  resolve_secrets : int option;
  prepare_executor : int option;
  prepare_script : int option;
  get_sources : int option;
  restore_cache : int option;
  step_script : int option;
  archive_cache : int option;
  cleanup_file_variables : int option;
}

let empty =
  {
    resolve_secrets = None;
    prepare_executor = None;
    prepare_script = None;
    get_sources = None;
    restore_cache = None;
    step_script = None;
    archive_cache = None;
    cleanup_file_variables = None;
  }

let parse_sections (trace : string) : t =
  let split_on_chars chars s =
    List.fold_left
      (fun ss char -> List.concat_map (String.split_on_char char) ss)
      [ s ] chars
  in
  let all_sections : (string, section_info) Hashtbl.t = Hashtbl.create 5 in
  let re = Rex.compile "section_(end|start):(\\d+):(\\w+)" in
  let index = ref 0 in
  List.iter
    (fun line ->
      match Rex.(line =~*** re) with
      | Some ("start", start_time, name) ->
          Hashtbl.replace all_sections name
            {
              index = !index;
              start_time = int_of_string start_time;
              duration = None;
            };
          incr index
      | Some ("end", end_time, name) -> (
          match Hashtbl.find_opt all_sections name with
          | Some { index; start_time; _ } ->
              let end_time = int_of_string end_time in
              let duration = end_time - start_time in
              Hashtbl.replace all_sections name
                { index; start_time; duration = Some duration }
          | None -> ())
      | Some _ -> ()
      | None -> ())
    (split_on_chars [ '\n'; '\r' ] trace);
  let get name =
    match Hashtbl.find_opt all_sections name with
    | Some section ->
        Hashtbl.remove all_sections name;
        section.duration
    | None -> None
  in
  {
    resolve_secrets = get "resolve_secrets";
    prepare_executor = get "prepare_executor";
    prepare_script = get "prepare_script";
    get_sources = get "get_sources";
    restore_cache = get "restore_cache";
    step_script = get "step_script";
    archive_cache = get "archive_cache";
    cleanup_file_variables = get "cleanup_file_variables";
  }

let sections_to_string
    {
      resolve_secrets;
      prepare_executor;
      prepare_script;
      get_sources;
      restore_cache;
      step_script;
      archive_cache;
      cleanup_file_variables;
    } : string =
  let pp =
    Option.fold ~none:"None" ~some:(fun d -> "Some " ^ string_of_int d)
  in
  sf
    "{resolve_secrets = %s; prepare_executor = %s; prepare_script = %s; \
     get_sources = %s; restore_cache = %s; step_script = %s; archive_cache = \
     %s; cleanup_file_variables = %s}"
    (pp resolve_secrets) (pp prepare_executor) (pp prepare_script)
    (pp get_sources) (pp restore_cache) (pp step_script) (pp archive_cache)
    (pp cleanup_file_variables)

let pp : Format.formatter -> t -> unit =
 fun fmt s -> Format.fprintf fmt "%s" (sections_to_string s)

let get_job_sections limiter_state ~token ~project ~job () : t Lwt.t =
  let open Lwt.Syntax in
  let uri = Gitlab.trace_uri ~project ~job () in
  let headers = Cohttp.Header.of_list [ ("Private-Token", token) ] in
  Log.debug "Fetching trace of job #%d from %s" job (Uri.to_string uri);
  let* resp, body =
    Limiter.get limiter_state (fun () -> Client.get ~headers uri)
  in
  match Cohttp.Response.status resp with
  | `OK ->
      let* trace = Cohttp_lwt.Body.to_string body in
      let sections = parse_sections trace in
      Log.debug "Got sections for job #%d: %s" job (sections_to_string sections);
      Lwt.return sections
  | _ ->
      raise
      @@ Invalid_argument
           (sf "Couldn't retrieve the trace of job #%d in project %s" job
              project)

let caqti_type : t Caqti_type.t =
  let encode = function
    | {
        resolve_secrets;
        prepare_executor;
        prepare_script;
        get_sources;
        restore_cache;
        step_script;
        archive_cache;
        cleanup_file_variables;
      } ->
        ( (resolve_secrets, prepare_executor, prepare_script, get_sources),
          (restore_cache, step_script, archive_cache, cleanup_file_variables) )
        |> Result.ok
  in
  let decode
      ( (resolve_secrets, prepare_executor, prepare_script, get_sources),
        (restore_cache, step_script, archive_cache, cleanup_file_variables) ) =
    {
      resolve_secrets;
      prepare_executor;
      prepare_script;
      get_sources;
      restore_cache;
      step_script;
      archive_cache;
      cleanup_file_variables;
    }
    |> Result.ok
  in
  Caqti_type.Std.(
    custom ~encode ~decode
      (tup2
         (tup4 (option int) (option int) (option int) (option int))
         (tup4 (option int) (option int) (option int) (option int))))
