This project implements a scraper for the [GitLab Jobs
API](https://docs.gitlab.com/ee/api/jobs.html) and storing the scraped
jobs in a SQLite3 database.

# Compiling

To compile the project, you need to install [opam](https://opam.ocaml.org/) and do e.g.:

```
opam switch create . 4.14.0
opam install . --deps-only
dune build
```

The resulting binary is available at `_build/install/default/bin/glsqlml`.

## Using Nix

Alternatively, using Nix:

```
nix --extra-experimental-features nix-command --extra-experimental-features flakes build
```

The resulting binary is available at `result/bin/glsqlml`.

# Usage

To run the scraper, invoke `glsqlml` at either `result/bin/glsqlml`
(if built using Nix) or at `_build/install/default/bin/glsqlml` (if
built directly through dune/opam).

By default, the scraper will index jobs from the last two weeks in the
project [tezos/tezos](https://gitlab.com/tezos/tezos/) and write them
to `gl.db`.

To index all jobs since a given date, do:

```
glsqlml --db jobs.db --index-from YYYY-MM-DD --token glpat-YOUR_TOKEN --project tezos/tezos
```

If `--index-from` is not given and the database is already populated,
then the scraper will index all new jobs since the last scrape. If
`--index-from` is not given and the database is empty, then it will
index jobs from the last two weeks.

Indexing requires a [GitLab
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens)
passed through `--token` or set in the `PRIVATE_TOKEN` environment
variable.

If `--db` is not given, it defaults to `gl.db`.

If `--project` is not given, it defaults to `tezos/tezos`.

If `--create-only` is given, an empty database is created with the latest schema.
If the specified database already exists, it is migrated to the latest schema.

To see the full list of options, do `dune exec bin/main.exe -- --help`.

After indexing, the resulting database can be queried using e.g. the `sqlite3` client:

```
$ sqlite3 jobs.db
```
# Database schema

The schema consists of a single table `jobs`. This table contains of
one row per scraped job. Each row has the columns:

 - `id` (`INTEGER`): job IDs
 - `json` (`TEXT`): the full JSON of the job as returned by the [GitLab List Project Jobs API](https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs).

The following columns correspond to the given JSON path in the returned JSON object per job:

 - `stage` (`TEXT`): `stage`
 - `status` (`TEXT`): `status`
 - `duration` (`REAL`): `duration`
 - `queued_duration` (`REAL`): `queued_duration`
 - `ref` (`TEXT`): `ref`
 - `created_at` (`TEXT`): `created_at`
 - `pipeline_id` (`INTEGER`): `pipeline.id`
 - `pipeline_source` (`TEXT`):: `pipeline.source`
 - `username` (`TEXT`): `user.username`
 - `web_url` (`TEXT`): `web_url`
 - `tag_list` (`TEXT`): the contents of `tag_list`, comma separated.
 - `failure_reason` (`TEXT`): `failure_reason`

If `--sections` is given during indexing, the following columns are
filled with the time spent, in seconds, in the corresponding
[section](TODO), during the jobs execution:

 - `resolve_secrets` (`INTEGER`):
 - `prepare_executor` (`INTEGER`):
 - `prepare_script` (`INTEGER`):
 - `get_sources` (`INTEGER`):
 - `restore_cache` (`INTEGER`):
 - `step_script` (`INTEGER`):
 - `archive_cache` (`INTEGER`):
 - `cleanup_file_variables` (`INTEGER`):

If `--sections` is not given during indexing, these columns are filled
with `NULL`.

If `--traces` is given during indexing, the column `trace` (`TEXT`)
will contain the trace of the job. If not, this column is filled with `NULL`.

```
$ sqlite3 jobs.db
SQLite version 3.37.2 2022-01-06 13:25:41
Enter ".help" for usage hints.
sqlite> .schema jobs
CREATE TABLE jobs (
    id INTEGER PRIMARY KEY NOT NULL,
    json TEXT NOT NULL,
    resolve_secrets INTEGER,
    prepare_executor INTEGER,
    prepare_script INTEGER,
    get_sources INTEGER,
    restore_cache INTEGER,
    step_script INTEGER,
    archive_cache INTEGER,
    cleanup_file_variables INTEGER,
    stage TEXT,
    name TEXT,
    status TEXT,
    duration REAL,
    queued_duration REAL,
    ref TEXT,
    created_at TEXT,
    pipeline_id INTEGER,
    pipeline_source TEXT,
    username TEXT,
    web_url TEXT,
    tag_list TEXT,
    failure_reason TEXT,
    trace TEXT,
);
sqlite> .mode line
sqlite> select * from jobs limit 1;
                    id = 5561948632
                  json = {"id":5561948632,"status":"success","stage":"trigger","name":" ....
       resolve_secrets = 0
      prepare_executor = 0
        prepare_script = 4
           get_sources = 9
         restore_cache =
           step_script = 1
         archive_cache =
cleanup_file_variables = 0
                 stage = trigger
                  name = success
                status = trigger
              duration =
       queued_duration =
                   ref = master
            created_at = 2023-11-24T16:00:27.727Z
           pipeline_id = 1084323012
       pipeline_source = push
              username = nomadic-margebot
               web_url = https://gitlab.com/tezos/tezos/-/jobs/5613038451
        failure_reason =
                 trace =
```

The JSON column can be dealt with using the [SQLite functions and
operators for JSON values](https://www.sqlite.org/json1.html), in
particular `json_extract`:

```
sqlite> .mode column
sqlite> select id, json_extract(json, "$.user.username") from jobs limit 5;
id          json_extract(json, "$.user.username")
----------  -------------------------------------
3961238592  romain.nl
3961238619  romain.nl
3961238623  romain.nl
3961238631  romain.nl
3961238643  romain.nl
```

# More examples

The file [examples/queries.sql](examples/queries.sql) contains a set of queries
demonstrating how to work with the data and the type of analysis that
can be performed.

# Known limitations

## Indexing

If the scraper is interrupted or encounters an error, then it will
write partial results to the database. If this happens when the
database already contains jobs, then the resulting data will contain
gaps.

For instance, let's say `gl.db` contains all the jobs created between
2022-01-01 -- 2022-02-01 for a given project.  Then, the scraper is
re-executed on 2022-03-01. The scraper will index and insert jobs in
the starting from 2022-03-01 working backwards towards 2022-02-01. If
it is interrupted before reaching 2022-02-01, say at 2022-02-15, then
the database will contain the jobs from the intervals:

  - 2022-01-01 -- 2022-02-01
  - 2022-02-15 -- 2022-03-01

Moreover, if the indexer is restarted then it cannot fill the gap, and
will only index up to 2022-03-01.

## Performance

Performance is quite poor due to the lack of database indexes.

## Only indexes jobs

We might want to index pipelines as well.

## Only supports project at a time

We might want to index multiple projects.
