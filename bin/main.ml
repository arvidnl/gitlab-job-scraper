(*****************************************************************************)
(*                                                                           *)
(* SPDX-License-Identifier: MIT                                              *)
(* Copyright (c) 2023 Nomadic Labs. <arvid.jakobsson@nomadic-labs.com>       *)
(*                                                                           *)
(*****************************************************************************)

open Cohttp_lwt_unix
open Lwt.Syntax
open Glsqlml
open Glsqlml.Util

let get ~token uri =
  let headers =
    Cohttp.Header.of_list
      [ ("Private-Token", token); ("Content-Type", "application/json") ]
  in
  let* resp, body = Client.get ~headers uri in
  Lwt.return (resp, body)

type cli_s = {
  mutable db : string;
  mutable index_from : float option;
  mutable project : string;
  mutable token : string option;
  mutable sections : bool;
  mutable scope : Gitlab.job_scope list option;
  mutable create_only : bool;
  mutable traces : bool;
}

let cli_options =
  {
    db = "gl.db";
    index_from = None;
    project = "tezos/tezos";
    token = Sys.getenv_opt "PRIVATE_TOKEN";
    sections = false;
    scope = None;
    create_only = false;
    traces = false;
  }

let setup_db () : (Caqti_lwt.connection, Caqti_error.t) Lwt_result.t =
  let open Lwt_result.Syntax in
  let initialize = not (Sys.file_exists cli_options.db) in
  let db_file_abs =
    if Filename.is_relative cli_options.db then
      Filename.(concat (Sys.getcwd ()) cli_options.db)
    else cli_options.db
  in
  let* (module Db : Caqti_lwt.CONNECTION) =
    Caqti_lwt.connect (Uri.of_string @@ "sqlite3://" ^ db_file_abs)
  in
  let* () =
    if initialize then (
      Log.info "Creating database %s" cli_options.db;
      Schema.create (module Db))
    else
      let* migration = Schema.migrate (module Db) in
      Lwt_result.return
      @@
      match migration with
      | None -> ()
      | Some (v, v') ->
          Log.info "Migrated database %s from V%d to V%d" cli_options.db
            (Db_version.to_int v) (Db_version.to_int v')
  in
  Lwt_result.return (module Db : Caqti_lwt.CONNECTION)

let get_job_trace limiter_state ~token ~project ~job () : string Lwt.t =
  let open Lwt.Syntax in
  let uri = Gitlab.trace_uri ~project ~job () in
  let headers = Cohttp.Header.of_list [ ("Private-Token", token) ] in
  Log.debug "Fetching trace of job #%d from %s" job (Uri.to_string uri);
  let* resp, body =
    Limiter.get limiter_state (fun () -> Client.get ~headers uri)
  in
  match Cohttp.Response.status resp with
  | `OK ->
      let* trace = Cohttp_lwt.Body.to_string body in
      Lwt.return trace
  | _ ->
      raise
      @@ Invalid_argument
           (sf "Couldn't retrieve the trace of job #%d in project %s" job
              project)

let index ~token (module Db : Caqti_lwt.CONNECTION) :
    (int, Caqti_error.t) Lwt_result.t =
  let open Lwt_result.Syntax in
  (*
     Find the latest id.
     Fetch rows up to latest id, or cli_options.index_from if set.
     Insert each such row
     Find all pipelines with a state that is:
      - created, pending, running, waiting_for_resource,
      - failed, success, canceled, skipped,
      - manual if no older than 3 days
  *)
  let limiter_state = Limiter.empty () in
  let* latest_opt = Db.find_opt Job.Q.latest () in
  let upsert, job_filter =
    let by_creation_date index_from (_id, json) =
      let created_at =
        ISO8601.Permissive.datetime JSON.(json |-> "created_at" |> as_string)
      in
      created_at >= index_from
    in
    match (cli_options.index_from, latest_opt) with
    | Some index_from, _ ->
        Log.info "Reindexing jobs created after %a."
          ISO8601.Permissive.pp_datetime index_from;
        (true, by_creation_date index_from)
    | None, Some latest_job ->
        Log.info "Scraping jobs with an id higher than %d (created_at=%s)"
          latest_job.id latest_job.created_at;
        (false, fun (id, _json) -> id > latest_job.id)
    | None, None ->
        let index_from = Unix.gettimeofday () -. (14.0 *. 24.0 *. 3600.0) in
        Log.info
          "Database is empty and [--index-from] is not given. Will index the \
           last two weeks, starting at %a."
          ISO8601.Permissive.pp_datetime index_from;
        (false, by_creation_date index_from)
  in
  let rec loop (inserted_total, oldest_created_at, oldest_id) next_link =
    Log.debug "Fetching next_link: %s" (Uri.to_string next_link);
    let* json, next_link_opt =
      let open Lwt.Syntax in
      let* resp, body =
        Limiter.get limiter_state (fun () -> get ~token next_link)
      in
      let next_link_opt =
        resp.headers |> Cohttp.Header.get_links
        |> List.find_opt (fun (link : Cohttp.Link.t) ->
               List.mem Cohttp.Link.Rel.next link.arc.relation)
      in
      let* json = body |> Cohttp_lwt.Body.to_string in
      Log.debug "Got json: %s"
        (if String.length json > 50 then String.sub json 0 50 ^ " ..." else json);
      Lwt_result.return
        (JSON.parse ~origin:(Uri.to_string next_link) json, next_link_opt)
    in
    let results =
      List.map
        (fun job -> (JSON.(job |-> "id" |> as_int), job))
        JSON.(json |> as_list)
    in
    let new_results, old_results = List.partition job_filter results in
    (match new_results with
    | (id, json) :: _ ->
        Log.debug "Now at id=#%d and created_at=%s" id
          JSON.(json |-> "created_at" |> as_string)
    | [] -> ());
    Log.debug "Got %d new_results, %d old_results" (List.length new_results)
      (List.length old_results);
    let* new_jobs =
      (* Optionally index sections *)
      if cli_options.sections || cli_options.traces then
        let open Lwt.Syntax in
        let* new_jobs =
          new_results
          |> Lwt_list.map_p @@ fun (id, json) ->
             let* trace =
               get_job_trace limiter_state ~project:cli_options.project ~token
                 ~job:id ()
             in
             let sections =
               if cli_options.sections then Job_sections.parse_sections trace
               else Job_sections.empty
             in
             let trace = if cli_options.traces then Some trace else None in
             Lwt.return (Job.of_json ~sections ~trace json)
        in
        Lwt_result.return new_jobs
      else
        Lwt_result.return
        @@ List.map
             (fun (_id, json) ->
               let sections = Job_sections.empty in
               let trace = None in
               Job.of_json ~sections ~trace json)
             new_results
    in
    let rec insert_all (affected, oldest_created_at, oldest_id)
        (jobs : Job.t list) : (int * string option * int option, _) Lwt_result.t
        =
      let open Lwt_result.Syntax in
      match jobs with
      | job :: results ->
          Log.debug "Inserting job #%d" job.id;
          (* also insert/upsert sections *)
          let* count =
            Db.exec_with_affected_count
              (if upsert then Job.Q.upsert else Job.Q.insert)
              job
          in
          insert_all
            ( count + affected,
              Some JSON.(job.json |-> "created_at" |> as_string),
              Some job.id )
            results
      | [] -> Lwt_result.return (affected, oldest_created_at, oldest_id)
    in
    let* inserted, oldest_created_at, oldest_id =
      insert_all (0, oldest_created_at, oldest_id) new_jobs
    in
    Log.debug "Inserted %d rows" inserted;
    let inserted_total = inserted_total + inserted in
    Log.info "%d jobs inserted, now at created_at=%s, id=%s %s(retries: %d)"
      inserted_total
      (Option.value ~default:"-" oldest_created_at)
      (Option.fold ~none:"-" ~some:string_of_int oldest_id)
      (match (cli_options.index_from, latest_opt) with
      | Some index_from, _ ->
          sf "(indexing jobs created after %s) "
            (ISO8601.Permissive.string_of_datetime index_from)
      | None, Some latest_job ->
          sf "(indexing to id %d, created_at=%s) " latest_job.id
            latest_job.created_at
      | None, None -> "")
      (Limiter.retries limiter_state);
    match next_link_opt with
    | Some next_link ->
        Log.debug "Got next link: %s" (Cohttp.Link.to_string next_link);
        if List.length new_jobs > 0 && List.length old_results = 0 then
          loop (inserted_total, oldest_created_at, oldest_id) next_link.target
        else (
          Log.debug
            "Found no new results or there were old_results: terminating \
             iteration";
          Lwt_result.return inserted_total)
    | None -> Lwt_result.return inserted_total
  in
  loop (0, None, None)
    Gitlab.(
      jobs_uri ~pagination:"keyset" ~per_page:200 ~order_by:"id" ~sort:"desc"
        ?scope:cli_options.scope ~project:cli_options.project ())

let main_create_only () : (unit, Caqti_error.t) Lwt_result.t =
  let open Lwt_result.Syntax in
  let* (_conn : Caqti_lwt.connection) = setup_db () in
  Lwt_result.return ()

let main_scrape ~token () : (int, Caqti_error.t) Lwt_result.t =
  let open Lwt_result.Syntax in
  let* conn = setup_db () in
  let* inserted_total =
    if cli_options.create_only then Lwt_result.return 0 else index ~token conn
  in
  Lwt_result.return inserted_total

let () =
  let speclist =
    Arg.align
      [
        ( "--db",
          Arg.String (fun db -> cli_options.db <- db),
          "<DB_FILE> Sqlite3 DB file." );
        ( "--index-from",
          Arg.String
            (fun index_from ->
              cli_options.index_from <-
                Some ISO8601.Permissive.(datetime ~reqtime:false index_from)),
          "<YYYY-MM-DD> Index jobs from this date and forwards." );
        ( "--project",
          Arg.String (fun project -> cli_options.project <- project),
          "<PROJECT_ID> GitLab project ID as a namespace/project string." );
        ( "--token",
          Arg.String (fun token -> cli_options.token <- Some token),
          "<TOKEN> GitLab API token." );
        ( "--verbose",
          Arg.Unit (fun () -> Log.set_level Debug),
          " Show debug output." );
        ( "--sections",
          Arg.Unit (fun () -> cli_options.sections <- true),
          " Collect section timing information. This creates an additional \
           GitLab API query per indexed job, and is thus slower and more \
           likely to be rate limited." );
        ( "--traces",
          Arg.Unit (fun () -> cli_options.traces <- true),
          " Collect job traces. This creates an additional GitLab API query \
           per indexed job, and is thus slower and more likely to be rate \
           limited." );
        ( "--terminated",
          Arg.Unit
            (fun () -> cli_options.scope <- Some Gitlab.terminated_job_scopes),
          " Only index terminated jobs." );
        ( "--create-only",
          Arg.Unit (fun () -> cli_options.create_only <- true),
          " Only create database, do not index." );
      ]
  in
  Arg.parse speclist
    (fun s ->
      Arg.(usage speclist (sf "No anonymous arguments (got %s)" s));
      exit 1)
    (sf "Usage: %s [options]\n\nOptions are:" Sys.argv.(0));
  match (cli_options.create_only, cli_options.token) with
  | false, None ->
      Arg.usage speclist "No token provided";
      exit 1
  | false, Some token ->
      Lwt_main.run
        (let* results = main_scrape ~token () in
         match results with
         | Ok inserted ->
             Log.info "Inserted %d new jobs" inserted;
             Lwt.return_unit
         | Error err ->
             Log.error "%a" Caqti_error.pp err;
             exit 1)
  | true, _ ->
      Lwt_main.run
        (let* results = main_create_only () in
         match results with
         | Ok () -> Lwt.return_unit
         | Error err ->
             Log.error "%a" Caqti_error.pp err;
             exit 1)
