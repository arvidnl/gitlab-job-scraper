# Changelog

## Development version

### Breaking changes

 - Add the columns `stage`, `status`, `name`, `duration`,
   `queued_duration`, `ref_`, `created_at`, `pipeline_id`,
   `pipeline_source`, `username`, and `web_url` to the `job` table.

### New features

 - Add `--verbose` flag enabling debug output.
 - Add `--terminated` flag restricting indexing to terminated jobs.
 - Add date to log timestamp.
 - Show the date we're indexing towards during indexing.
 - Add `--sections` flag to index job section duration.
 - Add `--traces` flag to index job traces.

### Bug fixes

 - Printed total is now correct, and goes through the logging system.
 - Print final errors through logging system.
 - `--token` is not required when `--create-only` is set.
