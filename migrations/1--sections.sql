ALTER TABLE jobs ADD COLUMN resolve_secrets INTEGER;


ALTER TABLE jobs ADD COLUMN prepare_executor INTEGER;


ALTER TABLE jobs ADD COLUMN prepare_script INTEGER;


ALTER TABLE jobs ADD COLUMN get_sources INTEGER;


ALTER TABLE jobs ADD COLUMN restore_cache INTEGER;


ALTER TABLE jobs ADD COLUMN step_script INTEGER;


ALTER TABLE jobs ADD COLUMN archive_cache INTEGER;


ALTER TABLE jobs ADD COLUMN cleanup_file_variables INTEGER;