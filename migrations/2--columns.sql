ALTER TABLE JOBS ADD COLUMN stage TEXT;


ALTER TABLE JOBS ADD COLUMN status TEXT;


ALTER TABLE JOBS ADD COLUMN name TEXT;


ALTER TABLE JOBS ADD COLUMN duration REAL;


ALTER TABLE JOBS ADD COLUMN queued_duration REAL;


ALTER TABLE JOBS ADD COLUMN ref TEXT;


ALTER TABLE JOBS ADD COLUMN created_at TEXT;


ALTER TABLE JOBS ADD COLUMN pipeline_id INTEGER;


ALTER TABLE JOBS ADD COLUMN pipeline_source TEXT;


ALTER TABLE JOBS ADD COLUMN username TEXT;


ALTER TABLE JOBS ADD COLUMN web_url TEXT;


ALTER TABLE JOBS ADD COLUMN tag_list TEXT;


UPDATE jobs
SET stage = json_extract(JSON, "$.stage"),
    status = json_extract(JSON, "$.status"),
    name = json_extract(JSON, "$.name"),
    duration = json_extract(JSON, "$.duration"),
    queued_duration = json_extract(JSON, "$.queued_duration"),
    REF = json_extract(JSON, "$.ref"),
          created_at = json_extract(JSON, "$.created_at"),
          pipeline_id = json_extract(JSON, "$.pipeline.id"),
          pipeline_source = json_extract(JSON, "$.pipeline.source"),
          username = json_extract(JSON, "$.user.username"),
          web_url = json_extract(JSON, "$.web_url"),
          tag_list = (IFNULL (
                                (SELECT group_concat(value)
                                 FROM jobs AS j2, json_each(j2.json, '$.tag_list')
                                 WHERE j2.id = jobs.id), "")) ;
